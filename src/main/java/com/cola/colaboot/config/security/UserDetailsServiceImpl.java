package com.cola.colaboot.config.security;

import com.cola.colaboot.module.system.service.SysAccessService;
import com.cola.colaboot.module.system.service.SysUserService;
import com.cola.colaboot.module.system.pojo.SysAccess;
import com.cola.colaboot.module.system.pojo.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private SysUserService userService;
    @Autowired
    private SysAccessService accessService;
    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        SysUser user = userService.getByUsername(userName);	//查找用户
        if(user == null){
            user = userService.getByPhone(userName);
            if(user == null){
                throw new UsernameNotFoundException("用户不存在");
            }
        }
        List<SysAccess> accesses = accessService.getAccessByUserName(user.getUsername());
        user.setAuthorities(accesses);
        return user;
    }
}
