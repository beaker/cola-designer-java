package com.cola.colaboot.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity  //里面已经有一个@Configuration注解
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private LoginFailHandler failHandler;
    @Autowired
    private JwtTokenFilter jwtTokenFilter;
    @Autowired
    LoginSuccessHandler successHandler;
    @Autowired
    private LogoutHandler logoutHandler;
    @Autowired
    private NoAccessHandler noAccessHandler;
    @Autowired
    private AuthFailHandler authFailHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        /*
         * 校验token是否合法
         */
        http.addFilterBefore(jwtTokenFilter, UsernamePasswordAuthenticationFilter.class);//UsernamePasswordAuthenticationFilter

        http.csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) //使用JWT,关闭session
                .and()
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers("/file/**","/sql/**","/design/getById/**","/design/authViewCode","/test/**").permitAll()
                .anyRequest()                                                     //其他url需要身份/权限认证
                .access("@RBACService.hasAccess(request, authentication)")
                .and()
                .exceptionHandling()                                          //授权失败/没有权限
                .authenticationEntryPoint(authFailHandler)
                .accessDeniedHandler(noAccessHandler)
                .and()
                .formLogin()
                .successHandler(successHandler)                                 //登录成功
                .failureHandler(failHandler)                                    //登录失败
                // .loginPage("/login")                                         //使用自定义登录页面
                //.loginProcessingUrl("/login").defaultSuccessUrl("/index", true).failureUrl("/login?error")    //登录成功后跳转到哪个页面
                .permitAll();
        //开启自动配置登录功能：没有登录/权限将跳转到/login请求返回一个默认的登录页面
        //登录请求为：/login
        http.formLogin();
        //开启自动注销配置
        http.logout()
//                .logoutSuccessUrl("/page/visitor")
                .logoutSuccessHandler(logoutHandler);//退出登录后返回到哪个页面,注销会删除cookie
        http.rememberMe();//开启记住密码功能，会发送给客户端一个cookie
    }
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public static void main(String[] args) {
        System.out.println(new BCryptPasswordEncoder().encode("0000"));
    }
}
