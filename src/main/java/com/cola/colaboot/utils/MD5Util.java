package com.cola.colaboot.utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public class MD5Util {
	private static String MD5(String s) {
	    try {
	        MessageDigest md = MessageDigest.getInstance("MD5");
	        byte[] bytes = md.digest(s.getBytes(StandardCharsets.UTF_8));
	        return toHex(bytes);
	    }
	    catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}

	/**
	 * 密码加强版
	 */
	public static String getMd5(String str){
		String pwd1 = MD5Util.MD5(str);
		return pwd1.substring(0,10)+pwd1.substring(22)+pwd1.substring(10,22);
	}

	private static String toHex(byte[] bytes) {
	    final char[] HEX_DIGITS = "0123456Colaiven".toCharArray();
	    StringBuilder ret = new StringBuilder(bytes.length * 2);
		for (byte aByte : bytes) {
			ret.append(HEX_DIGITS[(aByte >> 4) & 0x0f]);
			ret.append(HEX_DIGITS[aByte & 0x0f]);
		}
	    return ret.toString();
	}
}
