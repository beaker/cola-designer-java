package com.cola.colaboot.module.design.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cola.colaboot.module.design.pojo.DesignData;

public interface DesignDataMapper extends BaseMapper<DesignData> {
}
