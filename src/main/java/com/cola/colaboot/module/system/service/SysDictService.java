package com.cola.colaboot.module.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cola.colaboot.module.system.pojo.SysDict;

public interface SysDictService extends IService<SysDict> {
    IPage<SysDict> pageList(SysDict sysDict, Integer pageNo, Integer pageSize);

    SysDict getByTag(String tag);
}
