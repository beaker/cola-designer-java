package com.cola.colaboot.module.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cola.colaboot.module.system.mapper.SysRoleMapper;
import com.cola.colaboot.module.system.pojo.SysRole;
import com.cola.colaboot.module.system.service.SysRoleService;
import org.springframework.stereotype.Service;

@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {
    @Override
    public IPage<SysRole> pageList(SysRole sysRole, Integer pageNo, Integer pageSize) {
        LambdaQueryWrapper<SysRole> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(StringUtils.isNotBlank(sysRole.getRoleName()),SysRole::getRoleName,sysRole.getRoleName());
        IPage<SysRole> page = new Page<>(pageNo,pageSize);
        return page(page,queryWrapper);
    }
}
